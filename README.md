# Auckland Council EatSafe Grade Scraper

## Description

An open-source Python script that automatically scrapes Auckland Council's EatSafe restaurant grades website. Restaurant names, grades, expiry dates and locations are scraped and outputted in CSV format.

## Features

- Define one or more Auckland regions or locations to scrape restaurant EatSafe grades from.
- Extensive default list of locations included.
- Outputs CSV format, | delimited.
- Scrapes all result pages for each defined Auckland location.
- Not a 'mechanize'-based script, no headless browser required.


## Example CSV Output
| Name        | Location           | Grade  | Expiry | Version |
| ------------- |-------------| -----|-----|-----|
| Greg's Burgers    | 38 Fant Road, Panmure, Auckland 1071 | D | Grade expires 7 August 2021 | 20200207 |
| Pudding Palace   | 74 Half Mesa Drive, Parnell, Auckland 1042      |   A | Grade expires 23 April 2030 | 20200207
| Dos Sushi House | 4A Elizabeth Street, Mount Eden, Auckland 1032      |    A | Grade expires 23 January 2021 |20200207


## Requirements

- Python3
- Python packages: Requests, lxml, BeautifulSoup4

## How to Run
- Clone or download this repository.
- Optionally, edit line `134` in  `auckland_council_eatsafe_grade_scraper.py` with the desired Auckland locations you wish to scrape or use the Auckland-wide defaults.
- From your terminal or command prompt, run `python3 auckland_council_eatsafe_grade_scraper.py` to start scraping.
- Once the process has completed, an output CSV will be generated in the same directory.

## Acknowledgements and Notes

- All EatSafe grades are collected from [Auckland Council](https://www.aucklandcouncil.govt.nz).
- Released under GNU General Public License v3 (GPL-3).
- If you improve or adapt this script, including adding new Auckland locations, please consider contributing back to source so others can benefit.
