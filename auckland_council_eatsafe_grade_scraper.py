# Scrapes Auckland Council's EatSafe food / restaurant grades website and stores the result dataset in CSV format.
# https://www.aucklandcouncil.govt.nz/licences-regulations/business-licences/food-businesses-quality-grading/Pages/find-food-grade-restaurant-cafe.aspx
# Coster 2020
# coster@devcoster.com
# Python3

# Configure the variable 'areasToScrape' to define which Auckland areas you would like scrape the EatSafe food grades for. A default is provided.

# Dependencies
# lxml for bs4 is also required (e.g. pip3 install lxml)
from bs4 import BeautifulSoup
import requests
import re
import csv
import datetime
import time

# Main control loop used to scrape a single location or area.
# Loops until all defined areas have been scraped and stored.
def controlLoop(outputDeck, areasToScrape, formData, currentRelativePage):
    for items in areasToScrape:
        area = items
        print('')
        print('########################')
        print('Scraping area: ' + items)
        outputDeck += scrapePage(area, currentRelativePage, formData)

# Main function used to request and scrape a single result page for an area.
def scrapePage(area, currentRelativePage, formData):
    # Variables to store temporary state.
    currentRun = 0
    pages = 1
    global tempCards

    # Run loop while there are pages to process.
    # The number of pages will be defined once the first result page is scraped.
    while currentRun < int(pages):

        # How many loops / pages so far?
        currentRun += 1

        # Request grades for this area.
        soupResults = BeautifulSoup(sendRequest(area, currentRelativePage, formData, currentRun), 'lxml')

        # Capture the viewstate from the result page.
        # ASP.NET stores session information in a viewstate and is required by the server to produce a valid response.
        newViewState = re.search('%s(.*)%s' % (re.escape('__VIEWSTATE|'), re.escape('|8|')), soupResults.text).group(1)
        newViewStateValidation = re.search('%s(.*)%s' % (re.escape('__EVENTVALIDATION|'), re.escape('|100|')), soupResults.text).group(1)

        # Update the local viewstate with the recently captured one.
        formData['__VIEWSTATE'] = newViewState
        formData['__EVENTVALIDATION'] = newViewStateValidation

        # Figure out which the scrape is on...
        # The EatSafe site uses relative page numbers.
        reportedPage = soupResults.find('span', attrs={'class':'sr-only'}, string='Current page, page ')
        parentIdText = reportedPage.parent.get('id')
        imoPage = parentIdText.rsplit('_', 1)[0]
        imoPage = imoPage[-1]

        # Set next page to current + 1
        currentRelativePage = int(imoPage) + 1

        # Check if this is the first results page.
        if currentRun == 1:

            # Print total number of results found.
            resultCount = soupResults.findAll("div", {"class": "subheader m-b-3dot5"})
            count = resultCount[0].find('span').get_text()
            count = count.split()
            print('Food grades found: ', count[0])

            if int(count[0]) > 1:
                # Print number of result pages found for this area.
                resultPages = soupResults.findAll("p", {"class": "pull-md-left pagination-details"})
                pages = re.findall(r'\d+', resultPages[0].find('span').get_text())[-1]
                print('Result pages:', pages)
                print('########################')
                print('')

                # Save all results found on this page.
                tempCards = soupResults.findAll("div", {"class": "card card-content"})
            else:
                print('No results found!')
                tempCards = None

            # Print progress update
            print('Scraping page '  + str(currentRun) + ' out of ' + str(pages) + ' for ' + area)
        else:
            # Not the first page.
            # Slow scrape to a reasonable speed as to not spam the server.
            time.sleep(2)
            tempCards += soupResults.findAll("div", {"class": "card card-content"})
            print('Scraping page '  + str(currentRun) + ' out of ' + str(pages) + ' for ' + area)

    return tempCards

# Used to send a HTTP POST request to the EatSafe search page.
# Returned result should contain the grades for the requested area.
def sendRequest(area, currentRelativePage, formData, currentRun):
    # Get ready to send the request to Auckland Council's EatSafe search page for the current location / area.

    # There are some JavaScript parameters that need to be scraped, in addition to the viewstate mentioned above,
    # from the page before the next request can be made.
    # Use RegEx to capture the ASP.net digest which is dynamic.
    pattern = re.compile(r"formDigestElement.value = '(.*?)';", re.MULTILINE | re.DOTALL)
    script = soupTomato.find("script", text=pattern)
    digest = pattern.search(script.text).group(1)

    # Populate a few missing parameters required for the request that weren't easily found on the 'landing page' in plain HTML.
    # Where possible, just use blanks.
    formData['__REQUESTDIGEST'] = digest
    formData['ctl00$SPWebPartManager1$g_68706fd1_a9ad_4e8b_ad74_c1cf65bad1c2$ctl00$txtSearchTxt'] = area
    formData['ctl00$ScriptManager'] = 'ctl00$ScriptManager'
    formData['__ASYNCPOST'] = 'true'
    formData['__EVENTARGUMENT'] = ''

    # If this is the first run, omit the relative page parameter from the HTTP header, else add it.
    if currentRun == 1:
        formData['__EVENTTARGET'] = 'ctl00$SPWebPartManager1$g_68706fd1_a9ad_4e8b_ad74_c1cf65bad1c2$ctl00$btnsearch'
    else:
        formData['__EVENTTARGET'] = 'ctl00$SPWebPartManager1$g_68706fd1_a9ad_4e8b_ad74_c1cf65bad1c2$ctl00$rptPaging$ctl0' + str(currentRelativePage) + '$linkPage'

    # Set some general request headers, like a generic useragent, and request the page.
    returnData = requests.post(url, data = formData, cookies = {'WSS_FullScreenMode': 'false'}, headers = {'Host': 'www.aucklandcouncil.govt.nz','User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/71.0','Accept': '*/*','Accept-Language': 'en-NZ','Accept-Encoding': 'gzip, deflate, br','Referer': 'https://www.aucklandcouncil.govt.nz/licences-regulations/business-licences/food-businesses-quality-grading/Pages/find-food-grade-restaurant-cafe.aspx','X-Requested-With': 'XMLHttpRequest','X-MicrosoftAjax': 'Delta=false','Content-Type': 'application/x-www-form-urlencoded; charset=utf-8','Cookie': 'WSS_FullScreenMode=false'}).text

    return returnData

# URL for the EatSafe page.
url = "https://www.aucklandcouncil.govt.nz/licences-regulations/business-licences/food-businesses-quality-grading/Pages/find-food-grade-restaurant-cafe.aspx"
print("Attempting to scrape: " + url)

# Which Auckand areas should the scrape include?
areasToScrape = ['Albany', 'Bayswater', 'Bayview', 'Beach Haven', 'Belmont', 'Birkdale', 'Birkenhead', 'Browns Bay', 'Campbells Bay', 'Castor Bay', 'Chatswood', 'Cheltenham', 'Crown Hill', 'Cuthill', 'Devonport', 'Fairview Heights', 'Forrest Hill', 'Glenfield', 'Glenvar', 'Greenhithe', 'Hauraki', 'Highbury', 'Hillcrest', 'Long Bay', 'Mairangi Bay', 'Marlborough', 'Meadowood', 'Milford', 'Murrays Bay', 'Narrow Neck', 'Northcote', 'Northcote Central', 'Northcote Point', 'North Harbour', 'Northcross', 'Rosedale', 'Rothesay Bay', 'Okura', 'Oteha', 'Paremoremo', 'Pinehill', 'Schnapper Rock', 'Stanley Bay', 'Stanley Point', 'Sunnynook', 'Takapuna', 'The Palms', 'Torbay', 'Totara Vale', 'Unsworth Heights', 'Waiake', 'Wairau Valley', 'Westlake', 'Windsor Park', 'Glen Eden', 'Glendene', 'Green Bay', 'Henderson', 'Herald Island', 'Hobsonville', 'Huia', 'Karekare', 'Kelston', 'Konini', 'Laingholm', 'Lincoln', 'McLaren Park', 'Massey', 'New Lynn', 'Oratia', 'Ranui', 'Royal Heights', 'Sunnyvale', 'Swanson', 'Te Atatu', 'Te Atatu Peninsula', 'Te Atatu South', 'Titirangi', 'Westgate', 'West Harbour', 'Western Heights', 'Whenuapai', 'Dairy Flat', 'Gulf Harbour', 'Helensville', 'Manly', 'Millwater', 'Muriwai', 'Orewa', 'Red Beach', 'Riverhead', 'Sandspit', 'Silverdale', 'Stanmore Bay', 'Waimauku', 'Waiwera', 'Warkworth', 'Whangaparaoa', 'Wellsford', 'Puhoi', 'Te Hana', 'Matakana', 'Leigh', 'Auckland']

# Load the defined EatSafe page, ready for parsing.
getContent = requests.get(url).content
soup = BeautifulSoup(getContent, 'lxml')
form = soup.find('form')
fields = form.findAll('input')
soupTomato = BeautifulSoup(getContent, 'html.parser')

# The landing page contains a input form that we need to capture parameters from.
# Pull useful HTML input parameters out of the form and create a dictionary out of them.
# These will be modified and used to make the grades request (as required by the server).
formData = dict((field.get('name'), field.get('value')) for field in fields)

# Set currentPage progress counter to 0, the first page.
currentRelativePage = 0

# This is where the result set will be stored.
outputDeck = []

# Start scraping grades for selected areas...
controlLoop(outputDeck, areasToScrape, formData, currentRelativePage)

# If the scrape was successful, process the output and store the dataset in CSV format.
if outputDeck is not None:
    # Set CSV output parameters.
    csv.register_dialect('type', delimiter = '|', quoting=csv.QUOTE_NONE, escapechar='~')

    # Set CSV output path and format.
    date = datetime.datetime.today().strftime('%Y%m%d')
    filename = 'auckland_council_eatsafe_grades_' + date + '.csv'

    # Write returned grades to CSV with headers.
    print('Writing ' + str(len(outputDeck)) + ' items to CSV....')
    with open(filename, 'w', newline='') as f:
        writer = csv.writer(f, dialect='type')

        # Headers
        writer.writerow(['Name', 'Location', 'Grade', 'Expiry', 'Version'])

        # Content
        for div in outputDeck:
            name = div.find('h3').get_text()
            location = div.find('p', {'class': 'card-text'}).get_text()
            grade = div.find('p', {'class': 'key-value bold m-b-0'}).get_text()
            expiry = div.find('span', {'class': 'calendar p-l-1'}).get_text()

            # print('Name: ' + name)
            # print('Location: ' + location)
            # print('Grade: ' + grade)
            # print('Expires: ' + expiry)
            # print('')

            writer.writerow([name.replace('|',''), location.replace('|',''), grade.replace('|',''), expiry.replace('|',''), date])
    f.close()
    print('Completed writing to CSV: ' + filename)
    print('Job finished!')
else:
    print('Error - No output to process...')
